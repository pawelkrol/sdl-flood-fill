CHANGES
=======

Revision history for `sdl-flood-fill`, a proof of concept [SDL](https://www.libsdl.org/) application created to illustrate an implementation of a few basic graphic rendering algorithms.

0.01 (2016-05-14)
-----------------

* Initial version (provides an implementation of a flood-filled quadrangle drawing algorithm, which incorporates Bresenham's line drawing algorithm, an abstraction layer over SDL library dedicated to alleviate the hassle of rendering and animating any 2D plane on an SDL's canvas screen, and finally the main program, which sets up a quadrangle object data with vertices rotating along an ellipse around their given center points)
