DEBUG=-ggdb3
OPTIMIZE=-O3
ARGS=$(OPTIMIZE)

REQS=objects.h

DEPS=bresenham floodfill quadrangle sdlapp
LIBS=$(patsubst %, -l%, $(DEPS))
SRCS=$(patsubst %, %.c, $(DEPS))
OBJS=$(SRCS:%.c=lib%.so)

SDL_TRAPEZ=main
SDL_TRAPEZ_C=$(SDL_TRAPEZ).c
SDL_TRAPEZ_OBJ=$(SDL_TRAPEZ).obj

CC=gcc
CC_OPTS=-std=gnu99 -Wall -Werror $(ARGS)
CC_LIBS=-L. -lm -lSDL $(LIBS)

default: $(SDL_TRAPEZ)

%.o: %.c %.h $(REQS)
	$(CC) $(CC_OPTS) -fpic -c $< -o $@

lib%.so: %.o
	$(CC) -shared -o $@ $<
	$(RM) $<

$(SDL_TRAPEZ): $(SDL_TRAPEZ_C) $(OBJS)
	$(CC) $(CC_OPTS) -c $< -o $(SDL_TRAPEZ_OBJ)
	$(CC) $(CC_LIBS) -o $@ $(SDL_TRAPEZ_OBJ)
	$(RM) $(SDL_TRAPEZ_OBJ)

run: $(SDL_TRAPEZ)
	LD_LIBRARY_PATH=. ./$<

clean:
	$(RM) $(OBJS)
	$(RM) $(SDL_TRAPEZ)
