SDL Flood Fill
--------------

`sdl-flood-fill` is a proof of concept [SDL](https://www.libsdl.org/) application created to illustrate an implementation of a few basic graphic rendering algorithms: [Bresenham's line algorithm](https://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm) adopted to provide a line between two points, and [flood fill algorithm](https://en.wikipedia.org/wiki/Flood_fill) used to fill a particular bounded area with a given color by calling a recursion with 4 directions. The main program renders an animated quadrangle object on a 2D plane.

![Screenshot](https://bitbucket.org/pawelkrol/sdl-flood-fill/raw/master/screenshot.png)

Version
-------

Version 0.01 (2016-05-14)

Installation
------------

Simply run this command to build all prerequisite libraries and execute the main program:

    $ make && make run

SDL Abstraction Layer
---------------------

Building `libsdlapp.so` lets you simplify the process of setting up your own SDL application by providing a couple of supplementary functions that give you all the functionality you might need to render and animate any 2D plane on an SDL's canvas screen.

    $ make libsdlapp.so

The main purpose of a separate library like this is to provide a simple method to focus on designing actual algorithms rather than dealing with all SDL rendering details. I have specifically built it to help me out with prototyping of potential demo effects to be eventually implemented in [MOS 6510](http://en.wikipedia.org/wiki/MOS_Technology_6510) assembly, which may then end up appearing in one of my future Commodore 64 productions.

Including a header file named `sdlapp.h` in your program exposes a couple of helper functions for you to use:

`SDLApp sdlapp_init(Configuration configuration, DisplayMode mode)`

See `objects.h` for a definition of `Configuration` and `DisplayMode` data structures, which you may want to adapt according to your own specific needs. It will perform an initial setup of an SDL application, and return an object representing that application.

`void sdlapp_exec(SDLApp app, Uint32 **frames, uint frameCount)`

Upon completion of an application setup you are able to execute it. Two properties that need to be initialized beforehand are a list of animation frames and their count. Each rendered frame is simply a pixel buffer that is just copied over to a screen surface using plain `memcpy` function when displaying of the next animation frame is about to happen. Its size is currently depending on X and Y dimensions inferred from a provided `DisplayMode` multiplied by 4 (due to the fact that each pixel color is represented by 32 bits).

`Uint32 sdlapp_get_pixel(RendererParams *params, Uint8 x, Uint8 y)`

See `objects.h` for a definition of `RendererParams` data structure. It allows you to fetch a pixel color for given logical screen coordinates. There is an important distinction between logical and physical screen coordinates that needs to be mentioned. Application's `Configuration` lets you specify a `scale` factor for all your pixels (which might be useful in various situations, e.g. prototyping an image projected onto Comoodore 64's screen in a text mode: since it is as small as 40x25 pixels, it will sure be very convenient to automatically upscale it by a factor of 8 or even 16). This function operates on logical coordinates.

`void sdlapp_put_pixel(RendererParams *params, Uint8 x, Uint8 y, Uint32 color)`

Similarly to fetching a pixel color for given logical screen coordinates, you are able set a pixel using an arbitrary RGB color at a specified location.

`Uint32 *sdlapp_get_pixel_buffer(SDLApp app)`

This is a supplementary function returning a new copy of a screen's pixel buffer. It removes the hassle of computing pixel buffer size and allocating a necessary amount of memory to store it. You may then operate directly on individual pixels of a screen surface, and then append a prerendered frame to a list of frames displayed during application's runtime, as defined via `frames` argument to an `sdlapp_exec` call. Please make sure to `free` the obtained buffer before your program terminates.

Also do not forget to link your executable file against the library by adding `-lsdlapp` to your linker's options.

Copyright And Licence
---------------------

Copyright (C) 2016 Paweł Król

This software is distributed under the terms of the MIT license. See [LICENSE](https://bitbucket.org/pawelkrol/sdl-flood-fill/src/master/LICENSE.md) for more information.
