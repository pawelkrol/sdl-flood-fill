#include <math.h>

#include "bresenham.h"
#include "sdlapp.h"

LineXY get_line(Point2D p1, Point2D p2) {
    LineXY line;

    line.points = 0;
    line.num_points = 0;

    /* Calculate constants dx, and dy. */
    const int dx = abs(p2.x - p1.x);
    const int dy = abs(p2.y - p1.y);

    /* 1. Store the left endpoint in (x, y). */
    uint x, y, yn;

    /* Determine which point to use as start, which as end. */
    if (p1.x < p2.x) {
        x = p1.x;
        y = p1.y;
        yn = p2.y;
    }
    else {
        x = p2.x;
        y = p2.y;
        yn = p1.y;
    }

    /* Check whether line drawing happens towards top or bottom. */
    const int direction = y < yn ? 1 : -1;

    /* Line drawing for slopes in the range 0 < |m| < 1. */
    if (dx > dy) {

        /* We step along the x direction in unit steps and calculate successive y values nearest the line path. */
        const int steps = dx + 1;
        Point2D *points = malloc(sizeof(Point2D) * steps);

        /* 2. Plot the first point. */
        const Point2D point = { .x = x, .y = y };
        *(points + 0) = point;

        /* 3. Calculate constants two_dy, and two_dy_minus_two_dx. */
        const int two_dy = 2 * dy;
        const int two_dy_minus_two_dx = two_dy - 2 * dx;

        /* 4. Obtain the starting value for the decision parameter. */
        int p = two_dy - dx;

        /* 5. At each xk along the line, starting at k = 0, perform the test. */
        for (int k = 1; k <= steps - 1; k++) {

            x++;

            /* 6. If pk < 0, the next point to plot is (xk + 1, yk). */
            if (p < 0) {
                p += two_dy;
            }
            /* 7. Otherwise, the next point to plot is (xk + 1, yk + 1). */
            else {
                y += direction;
                p += two_dy_minus_two_dx;
            }

            const Point2D point = { .x = x, .y = y };
            *(points + k) = point;
        }

        line.num_points = steps;
        line.points = points;
    }

    /* Line drawing for slopes in the range |m| > 1. */
    else {

        /* We step along the y direction in unit steps and calculate successive x values nearest the line path. */
        const int steps = dy + 1;
        Point2D *points = malloc(sizeof(Point2D) * steps);

        /* 2. Plot the first point. */
        const Point2D point = { .x = x, .y = y };
        *(points + 0) = point;

        /* 3. Calculate constants two_dx, and two_dx_minus_two_dy. */
        const int two_dx = 2 * dx;
        const int two_dx_minus_two_dy = two_dx - 2 * dy;

        /* 4. Obtain the starting value for the decision parameter. */
        int p = two_dx - dy;

        /* 5. At each yk along the line, starting at k = 0, perform the test. */
        for (int k = 1; k <= steps - 1; k++) {

            y += direction;

            /* 6. If pk < 0, the next point to plot is (xk + 1, yk). */
            if (p < 0) {
                p += two_dx;
            }
            /* 7. Otherwise, the next point to plot is (xk + 1, yk + 1). */
            else {
                x += 1;
                p += two_dx_minus_two_dy;
            }

            const Point2D point = { .x = x, .y = y };
            *(points + k) = point;
        }

        line.num_points = steps;
        line.points = points;
    }

    return line;
}

void draw_line(RendererParams *params, Point2D p1, Point2D p2, Uint32 strokeColor) {
    LineXY line = get_line(p1, p2);

    for (int i = 0; i < line.num_points; i++) {
        Point2D *point = line.points + i;
        sdlapp_put_pixel(params, point->x, point->y, strokeColor);
    }

    free(line.points);
}
