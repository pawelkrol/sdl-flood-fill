#ifndef bresenham_h
#define bresenham_h

#include "objects.h"

/* Bresenham's Line Drawing Algorithm */
extern void draw_line(RendererParams *, Point2D, Point2D, Uint32);

#endif // bresenham_h
