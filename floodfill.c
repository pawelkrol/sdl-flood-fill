#include "floodfill.h"
#include "sdlapp.h"

void flood_fill(RendererParams *params, Point2D point) {
    Uint32 strokeColor = params->strokeColor;
    Uint32 fillColor = params->fillColor;

    uint x = point.x;
    uint y = point.y;

    Uint32 color = sdlapp_get_pixel(params, x, y);

    if (color == 0xffffffff) {
        return;
    }

    if (color != strokeColor && color != fillColor) {
        sdlapp_put_pixel(params, x, y, fillColor);

        Point2D nextPoint;

        nextPoint.x = x;
        nextPoint.y = y - 1;
        flood_fill(params, nextPoint);

        nextPoint.x = x;
        nextPoint.y = y + 1;
        flood_fill(params, nextPoint);

        nextPoint.x = x - 1;
        nextPoint.y = y;
        flood_fill(params, nextPoint);

        nextPoint.x = x + 1;
        nextPoint.y = y;
        flood_fill(params, nextPoint);
    }
}
