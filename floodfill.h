#ifndef floodfill_h
#define floodfill_h

#include "objects.h"

/* Quadrangle Flood Fill Algorithm */
extern void flood_fill(RendererParams *, Point2D);

#endif // floodfill_h
