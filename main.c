#include <math.h>

#include "quadrangle.h"
#include "sdlapp.h"

#define DELAY_100MS (33 / 10)
#define SCALE 3

#define MAX_SHIFT_X 0x10
#define MAX_SHIFT_Y 0x08
#define FRAME_COUNT 0x80

#define BACKGROUND_COLOR 0xfdf6e3
#define PEN_COLOR 0x002b36
#define BUCKET_COLOR 0xeee8d5

void render_quadrangle_frame(RendererParams *params) {
    uint *sinusData = params->sinusData;

    int frameIndex = params->frameIndex;

    Point2D p1 = { .x = 0x20, .y = 0x20 };
    Point2D p2 = { .x = 0x70, .y = 0x30 };
    Point2D p3 = { .x = 0x78, .y = 0xa0 };
    Point2D p4 = { .x = 0x08, .y = 0xb0 };

    uint *sin_x = sinusData + FRAME_COUNT * 0;
    uint *sin_y = sinusData + FRAME_COUNT * 1;

    p1.x += *(sin_x + frameIndex);
    p2.x -= *(sin_x + frameIndex);
    p3.x += *(sin_x + frameIndex);
    p4.x -= *(sin_x + frameIndex);

    p1.y -= *(sin_y + frameIndex);
    p2.y += *(sin_y + frameIndex);
    p3.y -= *(sin_y + frameIndex);
    p4.y += *(sin_y + frameIndex);

    draw_quadrangle(params, p1, p2, p3, p4);
}

uint *precompute_sinus() {
    uint value;
    uint *sinus = malloc(2 * FRAME_COUNT * sizeof(uint));

    for (int i = 0; i < FRAME_COUNT; i++) {

        value = round(MAX_SHIFT_X / 2 * sin(2 * M_PI * i / FRAME_COUNT));
        *(sinus + FRAME_COUNT * 0 + i) = value;

        value = round(MAX_SHIFT_Y / 2 * cos(2 * M_PI * i / FRAME_COUNT));
        *(sinus + FRAME_COUNT * 1 + i) = value;
    }

    return sinus;
}

void precompute_frames(SDLApp app, Uint32 **frames) {
    fprintf(stdout, "Initializing...");
    fflush(stdout);

    uint *sinusData = precompute_sinus();

    for (int i = 0; i < FRAME_COUNT; i++) {
        Uint32 *bufp = sdlapp_get_pixel_buffer(app);

        RendererParams params = {
            .app = app,
            .frameIndex = i,
            .strokeColor = PEN_COLOR,
            .fillColor = BUCKET_COLOR,
            .sinusData = sinusData,
            .bufp = bufp
        };

        render_quadrangle_frame(&params);

        *(frames + i) = bufp;
    }

    free(sinusData);

    fprintf(stdout, " done\n");
}

int main(int argc, char *argv[])
{
    Configuration configuration = {
        .background = BACKGROUND_COLOR,
        .frame_delay = DELAY_100MS,
        .scale = SCALE
    };
    DisplayMode mode = MULTI;

    SDLApp app = sdlapp_init(configuration, mode);

    Uint32 *frames[FRAME_COUNT];

    precompute_frames(app, frames);

    sdlapp_exec(app, frames, FRAME_COUNT);

    for (int i = 0; i < FRAME_COUNT; i++) {
        Uint32 *bufp = *(frames + i);
        free(bufp);
    }

    return 0;
}
