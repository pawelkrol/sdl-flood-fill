#ifndef objects_h
#define objects_h

#include <SDL/SDL.h>

typedef unsigned int uint;

struct PointXY {
    uint x;
    uint y;
};

typedef struct PointXY Point2D;

struct Line2D {
    Point2D *points;
    uint num_points;
};

typedef struct Line2D LineXY;

enum GraphicsMode { HIRES, MULTI, TEXT };

typedef enum GraphicsMode DisplayMode;

struct SDLApplication {
    Uint32 background;
    uint frame_delay;
    uint scale;
};

typedef struct SDLApplication Configuration;

struct RenderMode {
    Point2D dimensions;
    Point2D pixel_size;
};

typedef struct RenderMode GraphicsMode;

struct SDLRunner {
    Configuration configuration;
    GraphicsMode mode;
    SDL_Surface *screen;
};

typedef struct SDLRunner SDLApp;

struct SDLAppParams {
    SDLApp app;
    uint frameIndex;
    Uint32 strokeColor;
    Uint32 fillColor;
    uint *sinusData;
    Uint32 *bufp;
};

typedef struct SDLAppParams RendererParams;

struct TimerEventParams {
    SDLApp app;
    Uint32 **frames;
    uint frameCount;
    uint frameIndex;
};

typedef struct TimerEventParams CallbackParams;

#endif // objects_h
