#include <math.h>

#include "bresenham.h"
#include "quadrangle.h"
#include "floodfill.h"

#define AUTO_SHIFT_X 0x10

Point2D pseudo_intersection(Point2D p1, Point2D p2, Point2D p3, Point2D p4) {
    double x = (p1.x + p2.x + p3.x + p4.x) / 4;
    double y = (p1.y + p2.y + p3.y + p4.y) / 4;

    Point2D intersection = { .x = round(x), .y = round(y) };

    return intersection;
}

void draw_quadrangle(RendererParams *params, Point2D p1, Point2D p2, Point2D p3, Point2D p4) {
    p1.x += AUTO_SHIFT_X;
    p2.x += AUTO_SHIFT_X;
    p3.x += AUTO_SHIFT_X;
    p4.x += AUTO_SHIFT_X;

    draw_line(params, p1, p2, params->strokeColor);
    draw_line(params, p2, p3, params->strokeColor);
    draw_line(params, p3, p4, params->strokeColor);
    draw_line(params, p4, p1, params->strokeColor);

    Point2D intersection = pseudo_intersection(p1, p2, p3, p4);

    flood_fill(params, intersection);
}
