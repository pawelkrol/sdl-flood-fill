#ifndef quadrangle_h
#define quadrangle_h

#include "objects.h"

/* Flood-Filled Quadrangle-Drawing Algorithm */
extern void draw_quadrangle(RendererParams *, Point2D, Point2D, Point2D, Point2D);

#endif // quadrangle_h
