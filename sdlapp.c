#include "sdlapp.h"

#define BITS_PER_PIXEL 32
#define BYTES_PER_PIXEL BITS_PER_PIXEL / 8

GraphicsMode hires = {
    .dimensions = { .x = 40 * 8, .y = 25 * 8 },
    .pixel_size = { .x = 1, .y = 1 }
};

GraphicsMode multi = {
    .dimensions = { .x = 40 * 8, .y = 25 * 8 },
    .pixel_size = { .x = 2, .y = 1 }
};

GraphicsMode text = {
    .dimensions = { .x = 40 * 1, .y = 25 * 1 },
    .pixel_size = { .x = 1, .y = 1 }
};


uint max_x(SDLApp app) {
    uint max_x = app.mode.dimensions.x / app.mode.pixel_size.x;
    return max_x;
}

uint max_y(SDLApp app) {
    uint max_y = app.mode.dimensions.y / app.mode.pixel_size.y;
    return max_y;
}

uint dimension_x(SDLApp app) {
    uint dimension_x = max_x(app) * app.mode.pixel_size.x * app.configuration.scale;
    return dimension_x;
}

uint dimension_y(SDLApp app) {
    uint dimension_y = max_y(app) * app.mode.pixel_size.y * app.configuration.scale;
    return dimension_y;
}

GraphicsMode graphics_mode(DisplayMode mode) {
    GraphicsMode gfxMode;

    switch(mode) {
        case HIRES:
            gfxMode = hires;
            break;
        case MULTI:
            gfxMode = multi;
            break;
        case TEXT:
            gfxMode = text;
            break;
        default:
            fprintf(stderr, "Unable to init graphics mode: %u\n", mode);
            exit(1);
    }

    return gfxMode;
}

Uint32 sdlapp_color(SDLApp app, Uint32 hex) {
    Uint8 R = (hex & 0x00ff0000) >> 16;
    Uint8 G = (hex & 0x0000ff00) >> 8;
    Uint8 B = hex & 0x000000ff;

    Uint32 color = SDL_MapRGB(app.screen->format, R, G, B);

    return color;
}

size_t sdlapp_get_pixel_buffer_size(SDLApp app) {
    size_t size = sizeof(Uint32) * dimension_y(app) * app.screen->pitch / (BYTES_PER_PIXEL);

    return size;
}

Uint32 *sdlapp_screen_pixels(SDLApp app) {
    Uint32 *bufp = (Uint32 *)app.screen->pixels;

    return bufp;
}

void sdlapp_render_frame(CallbackParams *params) {
    SDLApp app = params->app;
    SDL_Surface *screen = app.screen;

    if (SDL_MUSTLOCK(screen)) {
        if (SDL_LockSurface(screen) < 0) {
            fprintf(stderr, "Unable to lock screen surface: %s\n", SDL_GetError());
            exit(1);
        }
    }

    SDL_FillRect(screen, NULL, sdlapp_color(app, app.configuration.background));

    Uint32 **frames = params->frames;

    if (frames != NULL) {
        Uint32 *frameBuffer = *(frames + params->frameIndex);
        Uint32 *bufp = sdlapp_screen_pixels(app);

        memcpy(bufp, frameBuffer, sdlapp_get_pixel_buffer_size(app));
    }

    if (SDL_MUSTLOCK(screen)) {
        SDL_UnlockSurface(screen);
    }
    SDL_UpdateRect(screen, 0, 0, dimension_x(app), dimension_y(app));
}

void sdlapp_set_transparency(SDLApp app, SDL_Surface *image) {
    Uint32 colorkey = sdlapp_color(app, app.configuration.background);
    if (SDL_SetColorKey(image, SDL_SRCCOLORKEY, colorkey)) {
        fprintf(stderr, "Unable to set transparency: %s\n", SDL_GetError());
        exit(1);
    }
}

SDLApp sdlapp_init(Configuration configuration, DisplayMode mode) {
    GraphicsMode gfxMode = graphics_mode(mode);

    if (SDL_Init(SDL_INIT_TIMER | SDL_INIT_VIDEO) < 0) {
        fprintf(stderr, "Unable to init SDL: %s\n", SDL_GetError());
        exit(1);
    }
    atexit(SDL_Quit);

    SDLApp app = {
        .configuration = configuration,
        .mode = gfxMode
    };

    uint dim_x = dimension_x(app);
    uint dim_y = dimension_y(app);

    SDL_WM_SetCaption("SDL Flood Fill", "SDL Flood Fill");

    SDL_Surface *screen = SDL_SetVideoMode(dim_x, dim_y, BITS_PER_PIXEL, SDL_SWSURFACE | SDL_DOUBLEBUF);
    if (screen == NULL) {
        fprintf(stderr, "Unable to set %dx%d video: %s\n", dim_x, dim_y, SDL_GetError());
        exit(1);
    }

    app.screen = screen;

    SDL_Surface *icon = SDL_LoadBMP("icon.bmp");
    // sdlapp_set_transparency(app, icon);
    SDL_WM_SetIcon(icon, NULL);

    CallbackParams params = { .app = app, .frames = NULL };
    sdlapp_render_frame(&params);

    return app;
}

Uint32 sdlapp_callback(Uint32 interval, void *param) {
    CallbackParams *params = (CallbackParams *)param;

    sdlapp_render_frame(params);

    if (!(++params->frameIndex % params->frameCount)) {
        params->frameIndex = 0;
    }

    SDL_UserEvent userevent;

    userevent.type = SDL_USEREVENT;
    userevent.code = 0;
    userevent.data1 = NULL;
    userevent.data2 = NULL;

    SDL_Event event;

    event.type = SDL_USEREVENT;
    event.user = userevent;

    SDL_PushEvent(&event);

    return interval;
}

void sdlapp_exec(SDLApp app, Uint32 **frames, uint frameCount) {
    CallbackParams params = {
        .app = app,
        .frames = frames,
        .frameCount = frameCount,
        .frameIndex = 0
    };

    SDL_TimerID timerId = SDL_AddTimer(app.configuration.frame_delay, sdlapp_callback, &params);

    int quit = 0;

    SDL_Event event;

    while (!quit) {
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                quit = 1;
            }
        }
    }

    SDL_FreeSurface(app.screen);
    SDL_RemoveTimer(timerId);
}

Uint32 sdlapp_get_pixel(RendererParams *params, Uint8 x, Uint8 y) {
    SDLApp app = params->app;

    GraphicsMode gfxMode = app.mode;
    SDL_Surface *screen = app.screen;
    uint scale = app.configuration.scale;

    if (x < 0 || y < 0 || x >= max_x(app) || y >= max_y(app)) {
        Uint32 color = 0xffffffff;
        return color;
    }

    Uint32 *bufp = (Uint32 *)params->bufp + y * gfxMode.pixel_size.y * scale * screen->pitch / (BYTES_PER_PIXEL) + x * gfxMode.pixel_size.x * scale;

    Uint8 R, G, B;
    SDL_GetRGB(*bufp, screen->format, &R, &G, &B);
    Uint32 color = (R << 16) | (G << 8) | B;

    return color;
}

void sdlapp_put_pixel(RendererParams *params, Uint8 x, Uint8 y, Uint32 color) {
    SDLApp app = params->app;

    if (x >= max_x(app) || y >= max_y(app))
        return;

    GraphicsMode gfxMode = app.mode;

    uint pixel_size_x = gfxMode.pixel_size.x;
    uint pixel_size_y = gfxMode.pixel_size.y;
    uint scale = app.configuration.scale;

    for (int i = 0; i < pixel_size_x * scale; i++) {
        for (int j = 0; j < pixel_size_y * scale; j++) {
            Uint32 *pixel = params->bufp + (y * pixel_size_y * scale + j) * app.screen->pitch / (BYTES_PER_PIXEL) + (x * pixel_size_x * scale + i);
            *pixel = sdlapp_color(app, color);
        }
    }
}

Uint32 *sdlapp_get_pixel_buffer(SDLApp app) {
    size_t size = sdlapp_get_pixel_buffer_size(app);

    Uint32 *bufp = sdlapp_screen_pixels(app);
    Uint32 *copy = malloc(size);

    memcpy(copy, bufp, size);

    return copy;
}
