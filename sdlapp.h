#ifndef sdlapp_h
#define sdlapp_h

#include "objects.h"

extern SDLApp sdlapp_init(Configuration, DisplayMode);

extern void sdlapp_exec(SDLApp, Uint32 **, uint);

extern Uint32 sdlapp_get_pixel(RendererParams *, Uint8, Uint8);

extern void sdlapp_put_pixel(RendererParams *, Uint8, Uint8, Uint32);

extern Uint32 *sdlapp_get_pixel_buffer(SDLApp);

#endif // sdlapp_h
